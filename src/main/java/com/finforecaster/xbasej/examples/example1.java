/*
    xBaseJ - Java access to dBase files
    Copyright 1997-2023 - Joe McVerry Raleigh NC USA

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 3 of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program; if not, write to the Free Software Foundation,
    Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
package com.finforecaster.xbasej.examples;

import com.finforecaster.xbasej.DBF;
import com.finforecaster.xbasej.fields.CharField;
import com.finforecaster.xbasej.fields.DateField;
import com.finforecaster.xbasej.fields.LogicalField;
import com.finforecaster.xbasej.fields.NumField;

import java.time.LocalDate;
import java.time.Month;

public class example1 {

    public static void main(String args[]) {

        try {
            // Create a new dbf file
            DBF aDB = new DBF("class.dbf", true);

            // Create the fields

            CharField classId = new CharField("classId", 9);
            CharField className = new CharField("className", 25);
            CharField teacherId = new CharField("teacherId", 9);
            CharField daysMeet = new CharField("daysMeet", 7);
            CharField timeMeet = new CharField("timeMeet", 4);
            NumField credits = new NumField("credits", 2, 0);
            LogicalField UnderGrad = new LogicalField("UnderGrad");
            DateField dateField = new DateField("date");

            // Add field definitions to database
            aDB.addField(classId);
            aDB.addField(className);
            aDB.addField(teacherId);
            aDB.addField(daysMeet);
            aDB.addField(timeMeet);
            aDB.addField(credits);
            aDB.addField(UnderGrad);
            aDB.addField(dateField);

            aDB.createIndex("classId.ndx", "classId", true, true); // true - delete ndx, true - unique index,
            aDB.createIndex("TchrClass.ndx", "teacherID+classId", true, false); // true - delete NDX, false - unique
            // index,
            System.out.println("index created");

            classId.put("JAVA10100");
            className.put("Introduction to JAVA");
            teacherId.put("120120120");
            daysMeet.put("NYNYNYN");
            timeMeet.put("0800");
            credits.put(3);
            UnderGrad.put(true);
            dateField.put(LocalDate.of(1975, Month.MARCH, 8));

            aDB.write();

            classId.put("JAVA10200");
            className.put("Intermediate JAVA");
            teacherId.put("300020000");
            daysMeet.put("NYNYNYN");
            timeMeet.put("0930");
            credits.put(3);
            UnderGrad.put(true);
            dateField.put(LocalDate.of(2017, Month.NOVEMBER, 1));

            aDB.write();

            classId.put("JAVA501");
            className.put("JAVA And Abstract Algebra");
            teacherId.put("120120120");
            daysMeet.put("NNYNYNN");
            timeMeet.put("0930");
            credits.put(6);
            UnderGrad.put(false);
            dateField.put(LocalDate.of(2023, Month.DECEMBER, 21));

            aDB.write();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

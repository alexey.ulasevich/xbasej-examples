/*
    xBaseJ - Java access to dBase files
    Copyright 1997-2023 - Joe McVerry Raleigh NC USA

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 3 of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program; if not, write to the Free Software Foundation,
    Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
package com.finforecaster.xbasej.examples;

import com.finforecaster.xbasej.DBF;
import com.finforecaster.xbasej.fields.CharField;
import com.finforecaster.xbasej.fields.LogicalField;

public class example3 {

	public static void main(String args[]) {

		try {
			// Create a new dbf file
			DBF aDB = new DBF("teacher.dbf", true);

			// Define fields
			CharField teacherId = new CharField("teacherId", 9);
			CharField teacherNm = new CharField("teacherNm", 25);
			CharField dept = new CharField("dept", 4);
			LogicalField tenure = new LogicalField("tenure");

			// Add fields to database
			aDB.addField(teacherId);
			aDB.addField(teacherNm);
			aDB.addField(dept);
			aDB.addField(tenure);

			aDB.createIndex("teacher.ndx", "teacherID", true, true); // true - delete NDX, false - unique ID
			System.out.println("index created");

			// fields are referenced through the database object
			aDB.getField("teacherId").put("120120120");
			aDB.getField("teacherNm").put("Joanna Coffee");

			aDB.getField("dept").put("0800");
			((LogicalField) aDB.getField("tenure")).put(true);

			aDB.write();

			// fields are referenced by their local variables
			teacherId.put("321123120");
			teacherNm.put("Juan Veldazou");

			dept.put("0810");
			tenure.put(true);

			aDB.write();

			// fields referenced both ways
			aDB.getField("teacherId").put("300020000");
			aDB.getField("teacherNm").put("Exal De' Cuau");

			dept.put("0810");
			tenure.put(false);

			aDB.write();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}

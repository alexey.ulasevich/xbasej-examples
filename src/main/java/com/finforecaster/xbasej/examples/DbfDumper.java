package com.finforecaster.xbasej.examples;

import com.finforecaster.xbasej.DBF;
import com.finforecaster.xbasej.fields.Field;

/**
 * Reads and prints all columns, all rows of the specified file.
 * 
 * @author clott
 *
 */
public class DbfDumper {

	public static void main(String args[]) {

		if (args.length != 1) {
			System.err.println("Usage: DbfDumper file.dbf");
			return;
		}

		try {
			DBF dbf = new DBF(args[0], DBF.READ_ONLY);

			// Fields index from 1
			for (Field fld: dbf.getFields()) {
				System.out.println(
						"Field " + fld + ": " + fld.getName() + ", type=" + fld.getType() + ", len=" + fld.getLength());
			}

			for (int i = 1; i <= dbf.getRecordCount(); i++) {
				dbf.read();
				System.out.println("Record " + i);
				for (Field fld : dbf.getFields()) {
					System.out.println(fld.getName() + "= >" + fld.get() + "<");
				}
			}

			dbf.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}

/*
    xBaseJ - Java access to dBase files
    Copyright 1997-2023 - Joe McVerry Raleigh NC USA

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 3 of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program; if not, write to the Free Software Foundation,
    Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
package com.finforecaster.xbasej.examples;

import com.finforecaster.xbasej.DBF;
import com.finforecaster.xbasej.fields.CharField;
import com.finforecaster.xbasej.fields.LogicalField;
import com.finforecaster.xbasej.fields.NumField;

public class example4 {

	public static void main(String args[]) {

		String dow[] = { "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat" };

		try {
			// Open dbf file
			DBF classDB = new DBF("class.dbf");

			// Define fields
			CharField classId = classDB.getField("classId");
			CharField name = classDB.getField("className");
			CharField teacher = classDB.getField("teacherId");
			CharField time = classDB.getField("timeMeet");
			NumField credits = classDB.getField("credits");
			LogicalField underGrad = classDB.getField("UnderGrad");
			CharField daysMeet = classDB.getField("daysMeet");

			DBF teacherDB = new DBF("teacher.dbf");
			teacherDB.useIndex("teacher.ndx");

			// Define fields
			CharField teacherName = teacherDB.getField("teacherNm");

			for (int i = 1; i <= classDB.getRecordCount(); i++) {
				classDB.read();
				if (underGrad.getBoolean()) // just show undergrad courses
				{
					System.out.println(name.get() + " id " + classId.get());
					System.out.print("   Meets at: " + time.get() + " on ");
					for (int j = 0; j < 7; j++) {
						if (daysMeet.get().charAt(j) == 'Y')
							System.out.print(dow[j] + " ");
					}
					System.out.println("");
					System.out.println("   Credits: " + credits.get());
					teacherDB.find(teacher.get());
					// this is not perfect for two reasons
					// first, not catching record not found exception
					// second, dbase logic requires returning a record with an equal or greater than
					// key value
					System.out.println("   Taught by: " + teacherName.get());
				} // end if undergrad test
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
